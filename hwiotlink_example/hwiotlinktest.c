/*
 * Copyright (c) 2021-2022 Talkweb Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "cmsis_os2.h"
#include "ohos_init.h"

#include <dtls_al.h>
#include <mqtt_al.h>
#include <oc_mqtt_al.h>
#include <oc_mqtt_profile.h>
#include "ohos_run.h"
#include "app_ethernet.h"


#define CONFIG_APP_SERVERIP "121.36.42.100"
#define CONFIG_APP_DEVICEID "62294fb0c4e6a958e3560e16_12345678" // 替换为注册设备后生成的deviceid
#define CONFIG_APP_DEVICEPWD "12345678"                         // 替换为注册设备后生成的密钥
#define CONFIG_APP_LIFETIME 60 // seconds
#define CONFIG_QUEUE_TIMEOUT (5 * 1000)
#define MSGQUEUE_COUNT 5 //16
#define MSGQUEUE_SIZE 10

#ifdef DEMO_TLS
#define CONFIG_APP_SERVERPORT       "8883"
#define CN_SECURITY_TYPE     EN_DTLS_AL_SECURITY_TYPE_CERT
#else
#define CONFIG_APP_SERVERPORT       "1883"
#define CN_SECURITY_TYPE     EN_DTLS_AL_SECURITY_TYPE_NONE
#endif

osMessageQueueId_t mid_MsgQueue; // message queue id
typedef enum {
    en_msg_cmd = 0,
    en_msg_report,
    en_msg_conn,
    en_msg_disconn,
} en_msg_type_t;

typedef struct {
    char* request_id;
    char* payload;
} cmd_t;

typedef struct {
    int lum;
    int temp;
    int hum;
} report_t;

typedef struct {
    en_msg_type_t msg_type;
    union {
        cmd_t cmd;
        report_t report;
    } msg;
} app_msg_t;

typedef struct {
    osMessageQueueId_t app_msg;
    int connected;
    int led;
    int motor;
} app_cb_t;
static app_cb_t g_app_cb;

static void deal_report_msg(report_t* report)
{
    oc_mqtt_profile_service_t service;
    oc_mqtt_profile_kv_t temperature;
    oc_mqtt_profile_kv_t humidity;
    oc_mqtt_profile_kv_t luminance;
    oc_mqtt_profile_kv_t led;
    oc_mqtt_profile_kv_t motor;

    if (g_app_cb.connected != 1) {
        return;
    }

    service.event_time = NULL;
    service.service_id = "Agriculture";
    service.service_property = &temperature;
    service.nxt = NULL;

    temperature.key = "Temperature";
    temperature.value = &report->temp;
    temperature.type = EN_OC_MQTT_PROFILE_VALUE_INT;
    temperature.nxt = &humidity;

    humidity.key = "Humidity";
    humidity.value = &report->hum;
    humidity.type = EN_OC_MQTT_PROFILE_VALUE_INT;
    humidity.nxt = &luminance;

    luminance.key = "Luminance";
    luminance.value = &report->lum;
    luminance.type = EN_OC_MQTT_PROFILE_VALUE_INT;
    luminance.nxt = &led;

    led.key = "LightStatus";
    led.value = g_app_cb.led ? "ON" : "OFF";
    led.type = EN_OC_MQTT_PROFILE_VALUE_STRING;
    led.nxt = &motor;

    motor.key = "MotorStatus";
    motor.value = g_app_cb.motor ? "ON" : "OFF";
    motor.type = EN_OC_MQTT_PROFILE_VALUE_STRING;
    motor.nxt = NULL;

    oc_mqtt_profile_propertyreport(NULL, &service);
    return;
}

// use this function to push all the message to the buffer
static int msg_rcv_callback(oc_mqtt_profile_msgrcv_t* msg)
{
    int ret = 0;
    char* buf;
    int buf_len;
    app_msg_t* app_msg;
    
    if ((msg == NULL) || (msg->request_id == NULL) || (msg->type != EN_OC_MQTT_PROFILE_MSG_TYPE_DOWN_COMMANDS)) {
        return ret;
    }

    buf_len = sizeof(app_msg_t) + strlen(msg->request_id) + 1 + msg->msg_len + 1;
    buf = malloc(buf_len);
    if (buf == NULL) {
        printf("%s %d\r\n",__func__,__LINE__);
        return ret;
    }
    app_msg = (app_msg_t*)buf;
    buf += sizeof(app_msg_t);

    app_msg->msg_type = en_msg_cmd;
    app_msg->msg.cmd.request_id = buf;
    buf_len = strlen(msg->request_id);
    buf += buf_len + 1;
    memcpy_s(app_msg->msg.cmd.request_id, buf_len, msg->request_id, buf_len);
    app_msg->msg.cmd.request_id[buf_len] = '\0';

    buf_len = msg->msg_len;
    app_msg->msg.cmd.payload = buf;
    memcpy_s(app_msg->msg.cmd.payload, buf_len, msg->msg, buf_len);
    app_msg->msg.cmd.payload[buf_len] = '\0';

    printf("%s %d %d %s\r\n",__func__,app_msg->msg_type,msg->msg_len,app_msg->msg.cmd.payload);
    ret = osMessageQueuePut(g_app_cb.app_msg, &app_msg, 0U, CONFIG_QUEUE_TIMEOUT);
    if (ret != 0) {
        printf("%s %d\r\n",__func__,__LINE__);
        free(app_msg);
    }
    printf("%s %d\r\n",__func__,__LINE__);
    return ret;
}

static void oc_cmdresp(cmd_t* cmd, int cmdret)
{
    oc_mqtt_profile_cmdresp_t cmdresp;
    ///< do the response
    cmdresp.paras = NULL;
    cmdresp.request_id = cmd->request_id;
    cmdresp.ret_code = cmdret;
    cmdresp.ret_name = NULL;
    printf("%s %d\r\n",__func__,__LINE__);
    (void)oc_mqtt_profile_cmdresp(NULL, &cmdresp);
}

///< COMMAND DEAL
#include <cJSON.h>
static void deal_cmd_msg(cmd_t* cmd)
{
    cJSON* obj_root;
    cJSON* obj_cmdname;

    int cmdret = 1;
    obj_root = cJSON_Parse(cmd->payload);
    if (obj_root == NULL) {
        oc_cmdresp(cmd, cmdret);
    }
    obj_cmdname = cJSON_GetObjectItem(obj_root, "command_name");
    if (obj_cmdname == NULL) {
        cJSON_Delete(obj_root);
    }
    oc_cmdresp(cmd, cmdret);
    return;
}
static int MQTT_RUN_FLAG = 0;
static int CloudMainTaskEntry(void)
{
    app_msg_t* app_msg;
    uint32_t ret;

    g_app_cb.app_msg = osMessageQueueNew(MSGQUEUE_COUNT, MSGQUEUE_SIZE, NULL);
    if (g_app_cb.app_msg == NULL) {
        printf("Create receive msg queue failed");
    }
    oc_mqtt_profile_connect_t connect_para;
    (void)memset_s(&connect_para, sizeof(connect_para), 0, sizeof(connect_para));

    connect_para.boostrap = 0;
    connect_para.device_id = CONFIG_APP_DEVICEID;
    connect_para.device_passwd = CONFIG_APP_DEVICEPWD;
    connect_para.server_addr = CONFIG_APP_SERVERIP;
    connect_para.server_port = CONFIG_APP_SERVERPORT;
    connect_para.life_time = CONFIG_APP_LIFETIME;
    connect_para.rcvfunc = msg_rcv_callback;
    connect_para.security.type = CN_SECURITY_TYPE;

    ret = oc_mqtt_profile_connect(&connect_para);
    printf("ret = %u\n", ret);
    if ((ret == (int)en_oc_mqtt_err_ok)) {
        g_app_cb.connected = 1;
        printf("oc_mqtt_profile_connect succed!\r\n");
    } else {
        printf("oc_mqtt_profile_connect faild!\r\n");
    }
    while (1) {
        app_msg = NULL;
        (void)osMessageQueueGet(g_app_cb.app_msg, (void**)&app_msg, NULL, 0xFFFFFFFF);
        if (app_msg != NULL) {
            switch (app_msg->msg_type) {
                case en_msg_cmd:
                    deal_cmd_msg(&app_msg->msg.cmd);
                    break;
                case en_msg_report:
                    deal_report_msg(&app_msg->msg.report);
                    break;
                default:
                    break;
            }
            free(app_msg);
        }
        LOS_TaskDelay(100);
     }
    return 0;
}

static int SensorTaskEntry(void)
{
    app_msg_t* app_msg;
    while(!g_app_cb.connected){
        LOS_TaskDelay(100);
    }
    while (1) {
        app_msg = malloc(sizeof(app_msg_t));
        // printf("SENSOR:lum:%.2f temp:%.2f hum:%.2f\r\n", 11.12, 11.14, 11.18);
        if (app_msg != NULL) {
            app_msg->msg_type = en_msg_report;
            app_msg->msg.report.hum = 10;//(int)data.Humidity;
            app_msg->msg.report.lum = 12;//(int)data.Lux;
            app_msg->msg.report.temp = 18;//(int)data.Temperature;
            if (osMessageQueuePut(g_app_cb.app_msg, &app_msg, 0U, CONFIG_QUEUE_TIMEOUT != 0)) {
                free(app_msg);
            }
        }
        LOS_TaskDelay(200);
    }
    return 0;
}

static void IotMainTaskEntry(void)
{
    link_main_task_entry();
    (void) osal_task_create("CloudMainTaskEntry",CloudMainTaskEntry,NULL,0x1000,NULL,22);
    (void) osal_task_create("SensorTaskEntry",SensorTaskEntry,NULL,0x1000,NULL,23);
}

static int twlink_run_flag = 0;
static void eth_enable_state_callBack(EthLinkState state)
{
    /* ETH连接断开*/
    if(state == STATE_UPDATE_LINK_DOWN){
        printf("ETH LINK STATE: DisConnected!\r\n");
    }
     /* ETH连接成功*/
    else if(state == STATE_UPDATE_LINK_UP){ 
        printf("ETH LINK STATE: Connected!\r\n");
        if(!twlink_run_flag){
            twlink_run_flag++;
            IotMainTaskEntry();
        }  
    }
}

void iotlink_example(void)
{
    ethernet_enable(eth_enable_state_callBack); //有线网络使能
}
OHOS_APP_RUN(iotlink_example);
