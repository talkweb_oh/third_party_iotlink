#ifndef __OHOS_IMP_H
#define __OHOSIMPL_H

#define OHOS_WAIT_FOREVER 0xFFFFFFFF
#define OHOS_SYS_MS_PER_SECOND 1000
#define OHOSCFG_BASE_CORE_TICK_PER_SECOND 100
#define OHOS_OK (0U)
typedef unsigned int UINT32;

#endif
