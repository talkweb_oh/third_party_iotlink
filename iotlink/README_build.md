# hw_iot_sdk_tiny使用说明

# 简述

iot_link是连接华为云IoTDA平台的SDK,SDK提供端云协同能力，集成了MQTT、LwM2M、CoAP、mbedtls、LwIP 全套 IoT 互联互通协议栈，且在这些协议栈的基础上，提供了开放 API，用户只需关注自身的应用，而不必关注协议内部实现细节，直接使用SDK封装的API，通过连接、数据上报、命令接收和断开四个步骤就能简单快速地实现与华为IoTDA平台的安全可靠连接。目前在niobe407模组上移植适配hw_iot_sdk_tiny，已完成mqtt互通协议的适配。

# 代码修改

1：修改third_party\lwip\src\api\netdb.c的lwip_gethostbyname（）函数

修改如下：

```
struct hostent *
lwip_gethostbyname(const char *name)
{
  err_t err;
  ip_addr_t addr;

  /* buffer variables for lwip_gethostbyname() */
  HOSTENT_STORAGE struct hostent s_hostent;
  HOSTENT_STORAGE char *s_aliases;
  HOSTENT_STORAGE ip_addr_t s_hostent_addr;
  HOSTENT_STORAGE ip_addr_t *s_phostent_addr[2];
  HOSTENT_STORAGE char s_hostname[DNS_MAX_NAME_LENGTH + 1];
  printf("%s %d \r\n",__func__,__LINE__);
  /* query host IP address */
  err = netconn_gethostbyname(name, &addr);
  if (err != ERR_OK) {
     LWIP_DEBUGF(DNS_DEBUG, ("lwip_gethostbyname(%s) failed, err=%d\n", name, err));
     #if LWIP_DNS_API_DECLARE_H_ERRNO                          /*修改部分*/
     h_errno = HOST_NOT_FOUND;           
     #endif                                                    /*修改部分*/
     return NULL;
   }
```

2：修改device\board\talkweb\niobe407\sdk\lwip_adapter\include\lwipopts.h

开放LWIP_SO_SNDTIMEO和LWIP_SO_RCVTIMEO功能

```
#undef LWIP_SO_RCVTIMEO
#define LWIP_SO_RCVTIMEO              1

#undef LWIP_SO_SNDTIMEO
#define LWIP_SO_SNDTIMEO              1
```



